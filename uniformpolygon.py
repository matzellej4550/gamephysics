# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 14:00:43 2019

@author: sinkovitsd
"""

from rotatingpolygon import RotatingPolygon
from vector2 import vector2
from math import sin, cos
import pygame

class UniformPolygon(RotatingPolygon):
    def __init__(self, density, pos, vel, angle, angvel, opoints, color=(0,0,0), gravity=(0,0)):
        #print("UniformPolygon")
        pos = vector2(pos)
        
        # Copy opoints
        n = len(opoints)
        op = []
        for p in opoints:
            op.append(vector2(p))
        
        # For each triangle,
        total_mass = 0
        com_total = vector2(0,0)
        total_momi = 0
        for i in range(n):
            # Calculate the area of the triangle and its mass = density*area.
            mass = density*(1/2)*(op[i] % op[i-1])
            # Calculate the centroid (center of mass) of the triangle.
            centroid = (op[i] + op[i-1])/3
            # Calculate the moment of inertia of the triangle.
            momi = (mass/6)*(op[i].mag2() + op[i-1].mag2()
                             + op[i]*op[i-1])
            # Add the mass*centroid to the center-of-mass total.
            com_total += mass*centroid
            # Add the moment of inertia to the moment of inertia total. (momi)
            total_momi += momi
            # Add the mass to the mass total. (mass)
            total_mass += mass
            
        # Calculate the center of mass (centroid) of the polygon 
        # by dividing the center-of-mass total by the total mass.
        com = com_total/total_mass
        
        # Fix mass and momi to make them positive.
        if total_mass < 0:
            total_mass *= -1
            total_momi *= -1
        # Calculate the moment of inertia about the center of mass (momi)
        # by subtracting the total mass times the magnitude of the centroid squared.
        total_momi -= total_mass*com.mag2()
        # Shift the opoints to be about the new center of mass by
        # subtracting the centroid vector from opoint.
        for p in op:
            p -= com
        # Shift the position to account for the previous step 
        # by adding the centroid vector, rotated by angle, to the original position.
        # (The rotation is necessary to account for any nonzero angle given.)
        pos += com.rotated(angle)        
        
        # Now we call __init__ for the RotatingPolygon parent class.  
        #print("mass", total_mass, "momi", total_momi)
        RotatingPolygon.__init__(self, total_mass, pos, vel, total_momi, 
                                 angle, angvel, op, color, gravity)
