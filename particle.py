# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 21:37:08 2019

@author: sinkovitsd
"""

from vector2 import vector2

class Particle:
    def __init__(self, mass, pos, vel=vector2(0,0), gravity=vector2(0,0)):
        #print("Particle")
        if mass == 0:
            self.invmass = 0
        else:
            self.invmass = 1.0/mass
        self.pos = vector2(pos)
        self.vel = vector2(vel)
        self.gforce = mass*vector2(gravity)
        self.force = vector2(self.gforce)
        self.interactions = []
        self.contacts = []
        
    def integrate(self, dt):
        self.vel += self.invmass*self.force*dt
        self.pos += self.vel*dt
        self.force = vector2(self.gforce)
        
    def add_force(self, force, pos=None):
        self.force += force
    
    def add_impulse(self, impulse, pos=None):
        self.vel += self.invmass*impulse

    def translate(self, disp):
        self.pos += disp

    def delete(self):
        for i in self.interactions:
            i.remove(self)
        for c in self.contacts:
            c.remove(self)
            
class RotatingParticle(Particle):
    def __init__(self, mass, pos, vel=vector2(0,0), momi=0, angle=0, angvel=0, gravity=vector2(0,0)):
        #print("RotatingParticle")
        Particle.__init__(self, mass, pos, vel, gravity)
        if momi == 0:
            self.invmomi = 0
        else:
            self.invmomi = 1.0/momi
        self.angle  = angle
        self.angvel = angvel
        self.torque = 0
        
    def integrate(self, dt):
        Particle.integrate(self, dt)
        self.angvel += self.invmomi*self.torque*dt
        self.angle  += self.angvel*dt
        self.torque = 0

    def add_force(self, force, pos=None):
        self.force += force
        if pos is not None:
            r = pos - self.pos
            self.torque += r % self.force
    
    def add_impulse(self, impulse, pos=None):
        #print("impulse", self.vel, self.angvel)
        self.vel += self.invmass*impulse
        if pos is not None:
            r = pos - self.pos
            self.angvel += self.invmomi*(r % impulse)
            #print("impulse", self.invmass*impulse, self.invmomi*(r % impulse), r)
            #print("impulse", self.vel, self.angvel)
        
    def add_torque(self, torque):
        self.torque += torque
    
