# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 15:04:17 2019

@author: Student
"""

from vector2 import vector2
from particle import Particle

class RotatingParticle(Particle):
    def __init__(self, mass, pos, vel=vector2(0,0), mom, angle, angvel, gravity=vector2(0,0)):
        super().__init__(self, mass, pos, vel, gravity)
        if mom == 0:
            self.invmom = 0
        else:
            self.invmom = 1.0/mom
            
        self.angle = angle
        self.angvel = angvel
        self.torque = 0
        self.force = vector2(self.gforce)
        self.interactions = []
        self.contacts = []
        
    def integrate(self, dt):
        self.vel += self.invmass*self.force*dt
        if abs(self.vel.x) < 3 and abs(self.vel.y) < 3:
            self.vel = vector2(0.0, 0.0)
        self.pos += self.vel*dt
        self.force = vector2(self.gforce)
        self.angvel vel * dt+= self.torque * invmom * dt 
        self.angle += self.ang
        self.torque = 0

    def delete(self):
        for i in self.interactions:
            i.remove(self)
        for c in self.contacts:
            c.remove(self)
    
    def add_force(self, force, pos = None):
        self.force += force
        if pos is not None:
            r = pos - self.pos
            self.torque += r%self.force