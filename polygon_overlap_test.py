# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 14:42:14 2019

@author: Student
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:28:21 2019

@author: sinkovitsd
"""

import pygame
from circle import Circle
from polygon import Polygon
from contact import contact_polygon_polygon
from vector2 import vector2
from random import uniform

WHITE = (255,255,255)
BLACK = (0,0,0)

def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    # list of objects in world
    poly1 = Polygon((400,300),
                    ((uniform(-300,300), uniform(-200,200)),
                     (uniform(-300,300), uniform(-200,200)),
                     (uniform(-300,300), uniform(-200,200)),
                    ), (0,0,255), 1)
    poly2 = Polygon((400,300),
                    ((uniform(-300,300), uniform(-200,200)),
                     (uniform(-300,300), uniform(-200,200)),
                     (uniform(-300,300), uniform(-200,200)),
                    ), (255,0,0), 1)

    # Main Loop
    done = False
    frame_rate = 60

    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            elif event.type == pygame.MOUSEBUTTONDOWN:
                poly1 = Polygon((400,300),
                                ((uniform(-300,300), uniform(-200,200)),
                                 (uniform(-300,300), uniform(-200,200)),
                                 (uniform(-300,300), uniform(-200,200)),
                                ), (0,0,255), 1)
                poly2 = Polygon((400,300),
                                ((uniform(-300,300), uniform(-200,200)),
                                 (uniform(-300,300), uniform(-200,200)),
                                 (uniform(-300,300), uniform(-200,200)),
                                ), (255,0,0), 1)
           
        
        # Draw objects to screen
        
        contacts = contact_polygon_polygon(poly1,poly2,[])
        if len(contacts) > 0: # If overlap detected
            screen.fill((255,255,0)) # YELLOW
            # Draw small black circle at contact point (penetrating vertex)
            pygame.draw.circle(screen, BLACK, contacts[0].pos.pygame(), 5)
            # Draw a thin black line from the contact point to the edge penetrated
            # (The length of the line is the penetration.)
            length = contacts[0].normal*contacts[0].penetration
            pygame.draw.line(screen, BLACK, contacts[0].pos.pygame(), contacts[0].pos + length)
            
        else:
            screen.fill(WHITE) 
        
        poly1.draw(screen)
        poly2.draw(screen)
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(frame_rate)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise