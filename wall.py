# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 15:20:25 2019

@author: Student
"""
from particle import Particle
from vector2 import vector2
import pygame

class Wall(Particle):
    def __init__(self, pos, normal, color=(0,0,0), length= 1000):
        Particle.__init__(self, 0,pos)
        self.color = color
        self.normal = normal.hat()
        self.length = length
        
    def draw(self, screen): 
        tangent = self.normal.perp()
        p1 = (self.pos + self.length*tangent).pygame()
        p2 = (self.pos - self.length*tangent).pygame()
        pygame.draw.line(screen, self.color, p1, p2)