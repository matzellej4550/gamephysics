# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 14:00:43 2019

@author: sinkovitsd
"""

from particle import RotatingParticle
from polygon import Polygon
from math import sin, cos

class RotatingPolygon(RotatingParticle, Polygon):
    def __init__(self, mass, pos, vel, momi, angle, angvel, opoints, color=(0,0,0), gravity=(0,0)):
        #print("RotatingPolygon")
        Polygon.__init__(self, pos, opoints, color)      
        RotatingParticle.__init__(self, mass, pos, vel, momi, angle, angvel, gravity)
        self.update()
        
    def integrate(self, dt):
        RotatingParticle.integrate(self, dt)
        self.update()
    
    def rotate(self, angle):
        self.angle += angle
        self.update()
    
    def update(self):
        cosangle = cos(self.angle)
        sinangle = sin(self.angle)
        for i in range(len(self.opoints)):
            # rotate point
            self.points[i].assign(self.opoints[i].rotated(sinangle, cosangle))
            # translate point
            self.points[i] += self.pos
            # set drawpoint as integer
            self.drawpoints[i] = self.points[i].pygame()
            # rotate normal
            self.normals[i].assign(self.onormals[i].rotated(sinangle, cosangle))
                