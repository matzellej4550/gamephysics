# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 14:56:49 2019

@author: Student
"""
#FIREWORKS

from circle import Circle 
from vector2 import vector2
import pygame as pg
from random import randint

#this is where the magic happens
#code inspired by pygame loops from previous projects
def main():
    pg.init()
    screen = pg.display.set_mode([600,800])
    clock = pg.time.Clock()
    bg_color = [0,0,0]
    done = False
    screen.fill(bg_color) 
    #add the title
    #font = pg.font.SysFont('Calibri', 95, False, False)
    #text = font.render("FireWorks", True, [255,255,255])
    #screen.blit(text, [100, 40])
    
    objects = []
            
    while not done:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                done = True
            elif event.type == pg.MOUSEBUTTONDOWN and event.button == 1:
                mouse_pos = event.pos
                #get x and y references so that we aren't referencing the array each time
                clickX = mouse_pos[0]
                clickY = mouse_pos[1]
                #(mass, pos, vel, radius, color=(0,0,0), gravity=vector2(0,0))
                for i in range(10):
                    ball = Circle(randint(0,200), vector2(clickX, clickY), vector2(randint(-200,200),randint(-200,200) - 50), 10, [randint(0,255),randint(0,255),randint(0,255)], vector2(0,200))
                    objects.append(ball)

        #check depth
        #first the lil depth boys
        for o in range(len(objects) -1, -1, -1):
            objects[o].draw(screen)
            objects[o].integrate(1/60)
            #this only needs to be done once
            if objects[o].pos.x > 600 or objects[o].pos.x < 0 or objects[o].pos.y > 800:
               objects.pop(o)
    
        #o.decay()

        pg.display.update()
        screen.fill([0,0,0])
        clock.tick(60)
    
    pg.quit()
        
if __name__ == "__main__":
    try:
        main()
    except:
        pg.quit()
        raise
