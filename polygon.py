# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 14:00:43 2019

@author: sinkovitsd
"""

from particle import Particle
from vector2 import vector2
import pygame

class Polygon(Particle):
    def __init__(self, pos, opoints, color=(0,0,0), width=0):
        #print("Polygon")
        Particle.__init__(self, 0, pos)
        self.color = color
        self.normal_length = 0
        self.width = width
        
        # Copy opoints
        n = len(opoints)
        self.opoints = []
        for p in opoints:
            self.opoints.append(vector2(p))
        
        # Calculate onormals
        self.onormals = []
        for i in range(n):
            self.onormals.append((self.opoints[i]-self.opoints[i-1]).perp().hat())
        
        # Check for convexity and fix onormals
        for i in range(n):
            npos = 0
            nneg = 0
            for p in self.opoints:
                d = (p - self.opoints[i])*self.onormals[i] 
                if d > 1e-13:
                    npos += 1
                if d < -1e-13:
                    nneg += 1
            if npos > 0:
                if nneg == 0:
                    self.onormals[i] *= -1
                else:
                    print("WARNING! Nonconvex polygons are not allowed.", self.onormals[i], npos, nneg)

        # Create blank points, normals, and drawpoints, then update them
        self.points = []
        self.normals = []
        self.drawpoints = []
        for i in range(n):
            self.points.append(vector2(0,0))
            self.normals.append(vector2(self.onormals[i]))
            self.drawpoints.append((0,0))
        Polygon.update(self) # not self.update() 
        # because I want to Polygon version of this, as the rest of the object 
        # (if a subclass) might not be constructed yet
        
    def update(self):
        for i in range(len(self.opoints)):
            # translate point
            self.points[i] = self.opoints[i] + self.pos
            # set drawpoint as integer
            self.drawpoints[i] = self.points[i].pygame()
            
    def translate(self, disp):
        self.pos += disp
        self.update()
    
    def draw(self, screen):
        pygame.draw.polygon(screen, self.color, self.drawpoints, self.width)
        # draw normals
        #pygame.draw.circle(screen, (0,0,0), self.pos.pygame(), 5)
        if self.normal_length > 0:
            for i in range(len(self.points)):
                pygame.draw.line(screen, (0,0,0), self.drawpoints[i], 
                                 (self.points[i] 
                                   + self.normal_length*self.normals[i]
                                 ).pygame())
                