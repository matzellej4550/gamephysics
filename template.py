# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:28:21 2019

@author: sinkovitsd
"""

import pygame
from random import randint, uniform, shuffle
from math import pi, sin, cos
from circle import Circle
from vector2 import vector2
import force

WHITE = (255,255,255)
BLACK = (0,0,0)

def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    gravity = vector2(0,200) # Downward uniform gravity, set to zero
    G = 1000000 # Gravity between objects
    
    # list of objects in world
    objects = []
    # list of forces active    
    forces = []
    
    objects.append(Circle(9, vector2(400-30,300), vector2(0,14), 60, (255,127,0), gravity))
    objects.append(Circle(1, vector2(400+270,300), vector2(0,-126), 20, (127,127,127), gravity))
    #objects.append(Circle(1, vector2(400,300), vector2(0,-126), 20, (127,127,127), gravity))
    #forces.append(force.PairForce(objects, lambda o1,o2: force.gravity_force(o1,o2,G)))      
    dragc = 0.5
    forces.append(force.SingleForce(objects, lambda o: force.linear_drag(o, dragc)))
    #stiffness for spring
    k = 1
    #natural length for the spring force
    l = 4
    forces.append(force.PairForce(objects, lambda o1, o2: force.spring_force(o1, o1, o2, k, l)))
    #I tried to have a chain of springs, but couldn't figure out lambda
    #forces.append(force.PairForce(objects, lambda o2, o3: force.spring_force(o2, o2, o3, k, l)))
    
    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
        
        #Set main object to the mouse position
        objects[0].pos.x = pygame.mouse.get_pos()[0]
        objects[0].pos.y = pygame.mouse.get_pos()[1]
        
        # Add forces
        for f in forces:
            f.force_all()
        
        # Move objects
        for o in objects:
            o.integrate(dt)
        
        # Draw objects to screen
        screen.fill(WHITE) # clears the screen
        for o in objects:
            o.draw(screen)
            pygame.draw.line(screen, (0,0,0), vector2(objects[0].pos.x, objects[0].pos.y), vector2(objects[1].pos.x, objects[1].pos.y), 3)
            
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise