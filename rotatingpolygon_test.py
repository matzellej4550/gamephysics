# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:28:21 2019

@author: sinkovitsd
"""

import pygame
from random import randint, uniform, shuffle
from math import pi, sin, cos
from circle import Circle
from wall import Wall
from particle import Particle
from contact import contact_circle_polygon
from rotatingpolygon import RotatingPolygon
from uniformpolygon import UniformPolygon
from vector2 import vector2

WHITE = (255,255,255)
BLACK = (0,0,0)

def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    # list of objects in world
    objects = []
    
    gravity = vector2(0,90) # Downward uniform gravity, set to zero
    impulse = vector2(0,-1500000)
   
    #poly = RotatingPolygon(1,(400,300),(0,0),1,0,-1,((-100,0),(0,-50),(100,0),(0,50)), (255,0,0))
    poly2 = UniformPolygon(1,(600,300),(0,0),1,0,
                                   ((-100,0),(0,-50),(0,50)), (0,255,0), gravity)
    poly3 = UniformPolygon(1,(600,20),(0,0),1,0,
                                   ((-80,0),(0,-50),(50,-50),(80,0),(0,50)), (0,255,255), gravity)
    poly1 = UniformPolygon(1,(200,300),(0,0),1,0,
                                   ((-100,0),(0,-40),(100,0),(0,40)), (255,255,0), gravity)
    
    objects.append(poly2)
    objects.append(poly3)
    objects.append(poly1)

    #print("mass", 1.0/poly2.invmass)
    #print("pos ", poly2.pos)
    #print("vel ", poly2.vel)
    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            # left mouse click
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                point = Circle(0, event.pos, (0,0), 0)
                if len(contact_circle_polygon(poly2, point)) > 0:
                    poly2.add_impulse(impulse, vector2(event.pos))
                if len(contact_circle_polygon(poly3, point)) > 0:
                    poly3.add_impulse(impulse, vector2(event.pos))
                if len(contact_circle_polygon(poly1, point)) > 0:
                    poly1.add_impulse(impulse, vector2(event.pos))
                    
            
        # Move objects
        for o in objects:
            o.integrate(dt)
            
                 
        # Draw objects to screen
        screen.fill(WHITE) # clears the screen
        for o in objects:
            #print(o.pos)
            o.draw(screen)
            if o.pos.y > 560:
                # tell the player they lost
                font = pygame.font.SysFont('Calibri', 25, False, False)
                text = font.render("You have lost!", True, BLACK)
                screen.blit(text, [100, 100])
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
   
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise