# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 15:12:37 2019

@author: Student
"""

import pygame
from random import randint, uniform, shuffle
from math import pi, sin, cos
from uniformpolygon import UniformPolygon
from polygon import Polygon
from wall import Wall
from circle import Circle
from vector2 import vector2
import contact
from contact import contact_circle_polygon
import colorsys

WHITE = (255,255,255)
BLACK = (0,0,0)

def random_hsv_color(hlo, hhi, slo, shi, vlo, vhi):
    if hhi <  hlo:
        hhi += 1
    h = uniform(hlo, hhi)%1
    s = uniform(slo, shi)
    v = uniform(vlo, vhi)
    r, g, b = colorsys.hsv_to_rgb(h,s,v)
    return int(255*r), int(255*g), int(255*b)

def main():
    pygame.init()
    screen = pygame.display.set_mode([800,600])
    
    clicked = False
    
    gravity = vector2(0,500) # Downward uniform gravity, set to zero
    
    # list of objects in world
    objects = []
    # list of forces active    
    forces = []
    # list of contact generators
    contact_generators = []
    # list of contacts to be resolved
    contacts = []
    
    #objects.append(Circle(9, vector2(400-30,300), vector2(0,1), 60, (255,127,0), gravity))
    for n in range(10):
        objects.append(UniformPolygon(1e-4, vector2(randint(100,700),randint(0,500)), 
                                      vector2(0,0), uniform(-pi,pi), 0, 
                                      [vector2(0, 100),
                                       vector2(0, 0),
                                       vector2(100, 0),
                                       vector2(100, 100)],
                                      random_hsv_color(0,1,1,1,1,1), gravity))
    
    #print(1/objects[0].invmass, 1/objects[0].invmomi)    
    objects.append(Wall(vector2(0,600), vector2(0,-1)))
    objects.append(Wall(vector2(0,600), vector2(1,0)))
    objects.append(Wall(vector2(800,600), vector2(-1,0)))
    contact_generators.append(contact.ContactGenerator(objects, 0.01, 0.7, 0.8))
    # restitution, kinetic_friction, static_friction

    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate
    
    # obj reference


    previousMouse = vector2(0,0)
    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            # when we click, see if we are overlapping an obj
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                point = Circle(0, event.pos, (0,0), 0)
                for obj in objects:
                    if isinstance(obj, Polygon):
                        if len(contact_circle_polygon(obj, point)) > 0:
                            clicked = True

            elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                point = Circle(0, event.pos, (0,0), 0)
                for obj in objects:
                    if isinstance(obj, Polygon):
                        if len(contact_circle_polygon(obj, point)) > 0:
                            print("Obj vel changed. Prevmouse is ", previousMouse)
                            print("Obj vel changed. Obj.pos is ", obj.pos)
                            obj.vel = (previousMouse - obj.pos)/(dt)
                            print("New Vel is ", obj.vel)
                clicked = False
        # when we lift up, let go of selected obj
        # elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
        if clicked:
            
            point = Circle(0, event.pos, (0,0), 0)
            for obj in objects:
                if isinstance(obj, Polygon):
                        if len(contact_circle_polygon(obj, point)) > 0:
                            mouse = vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
                            obj.pos = mouse
                            obj.angvel = 0
                            obj.force = vector2(0,0)
                            previousMouse = vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
                
                        
        # Add forces
        for f in forces:
            f.force_all()
        
        # Move objects
        for o in objects:
            o.integrate(dt)
        
        # Get contacts
        niterations = 0
        max_iterations = 5
        lines = []
        while niterations < max_iterations:
            niterations += 1
            contacts = []
            for g in contact_generators:
                contacts.extend(g.contact_all())
            if len(contacts)==0:
                break
                
            # Resolve contacts
            contacts.sort(key=lambda x: x.penetration)
            for c in contacts:
                J = c.resolve()
                # Store a line of impulse to be drawn later
                if J is not None:
                    lines.append([c.pos, J])

        # Draw objects to screen
        screen.fill(WHITE) # clears the screen
        for o in objects:
            #print(o.pos)
            o.draw(screen)
        for line in lines:
            pygame.draw.line(screen, BLACK, line[0], (line[0] + 1*line[1]).pygame())
            pygame.draw.line(screen, BLACK, line[0], (line[0] - 1*line[1]).pygame())
            
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise