# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 11:14:47 2019

@author: sinkovitsd
"""
from vector2 import vector2
from circle import Circle
from polygon import Polygon
from wall import Wall
from math import sqrt
from math import copysign

def sgn(x):
    if x > 0:
        return 1
    if x < 0:
        return -1
    return 0

class Contact:
    def __init__(self, obj1, obj2, data, normal, penetration, pos=None, wall_pos=None):
        self.obj1 = obj1
        self.obj2 = obj2
        self.normal = normal
        self.penetration = penetration
        self.pos = pos
        self.wall_pos = wall_pos
        self.data = data
        
    def resolve(self):
        if self.pos is not None and self.wall_pos is not None:
            self.penetration = (self.wall_pos - self.pos)*self.normal
        restitution = self.data['restitution']
        friction = self.data['friction']
        static = self.data['static']
        
        # Resolve penetration
        if self.penetration > 0:
            total_invmass = self.obj1.invmass + self.obj2.invmass
            if total_invmass > 0:
                nudge = self.penetration/total_invmass
                self.obj1.translate(self.obj1.invmass*nudge*self.normal)
                self.obj2.translate(-self.obj2.invmass*nudge*self.normal)
                
        # Resolve velocity
        vel1 = vector2(self.obj1.vel)
        vel2 = vector2(self.obj2.vel)
        if hasattr(self.obj1, "angvel"):
            s1 = self.pos - self.obj1.pos
            vel1 += self.obj1.angvel % s1
        if hasattr(self.obj2, "angvel"):
            s2 = self.pos - self.obj2.pos
            vel2 += self.obj2.angvel % s2
        sep_vel = (vel1 - vel2)*self.normal
        if sep_vel < 0: # if moving toward one another
            if static == 0: 
                # Collisions WITHOUT friction
                target_sep_vel = -restitution*sep_vel
                delta_vel = target_sep_vel - sep_vel
                total_invmass = self.obj1.invmass + self.obj2.invmass
                ''' Add to total_invmass those extra terms involving rotation:
                    invmomi * (s % normal)**2 
                    Define s1 and s2 for use here and later.
                    Use if hasattr(self.obj, "invmomi") to check if rotatable.'''
                if hasattr(self.obj1, "invmomi"):
                    sn = s1 % self.normal
                    total_invmass += self.obj1.invmomi*sn*sn
                if hasattr(self.obj2, "invmomi"):
                    sn = s2 % self.normal
                    total_invmass += self.obj2.invmomi*sn*sn
                if total_invmass > 0:
                    impulse = delta_vel/total_invmass*self.normal
                    self.obj1.add_impulse(impulse, self.pos)
                    self.obj2.add_impulse(-impulse, self.pos)
                    return impulse # returns impulse for visualization purposes
            else: 
                # Collisions WITH friction
                # Abbreviations for normal and tangential
                n = self.normal
                t = n.perp()

                # Initial relative velocity and normal and tangential components
                vi = vel1 - vel2
                vin = vi*n
                vit = vi*t
                
                # This gets started the matrix W
                total_invmass = self.obj1.invmass + self.obj2.invmass               
                Wtt = total_invmass
                Wnn = total_invmass
                Wnt = 0
                ''' (1) Replace 0s with correct values.  
                    Define s1n, s1t, s2n, s2t, to simplify the expressions.'''
                s1 = self.pos - self.obj1.pos 
                s2 = self.pos - self.obj2.pos
                s1n = s1 * n
                s1t = s1 * t
                s2n = s2 * n
                s2t = s2 * t
                if hasattr(self.obj1, "invmomi"):
                    Wtt += self.obj1.invmomi*s1t*s1t
                    Wnn += self.obj1.invmomi*s1n*s1n
                    Wnt += self.obj1.invmomi*s1n*s1t
                if hasattr(self.obj2, "invmomi"):
                    Wtt += self.obj2.invmomi*s2t*s2t
                    Wnn += self.obj2.invmomi*s2n*s2n
                    Wnt += self.obj2.invmomi*s2n*s2t
                
                # Target velocity change for perfect static friction
                dvn = -(1 + restitution)*vin
                dvt = -vit
                
                ''' (2) Compute the normal component of impulse, Jn, needed 
                    to achieve the normal velocity change, dvn. '''
                Qn = Wnn*dvn + Wnt*dvt      
                Jn = Qn/(Wnn*Wtt - Wnt*Wnt)
                
                
                ''' (3) Compute mu as friction coefficient needed 
                    to achieve the required dvt, where Jt = mu*Jn. '''  
                Qt = Wnt*dvn + Wtt*dvt  
                #new
                if Jn != 0:
                    mu = Qt/Qn
                    if abs(mu) > static:
                        mu = copysign(friction, mu)
                        Jn = dvn/(Wtt - mu*Wnt)
                else:
                    mu = 0
                
                Jt = mu*Jn      
                ''' (4) If mu is too large for the friction coefficients, 
                    reduce it to an acceptable value. '''
                if mu*Jn*(vit + 0.5*(-Wnt + mu*Wnn)*Jn) > 0:
                    mu = 0
                    Jn = dvn/Wtt
                ''' (5) Compute impulse as the sum of normal and tangential 
                    components, Jn and Jt. '''
                if Jn > 0:
                    impulse = Jn*(n + mu*t)
                    self.obj1.add_impulse(impulse, self.pos)
                    self.obj2.add_impulse(-impulse, self.pos)
                
                if hasattr(self.obj1, "angvel"):
                    s1 = self.pos - self.obj1.pos
                    vel1 += self.obj1.angvel % s1
                if hasattr(self.obj2, "angvel"):
                    s2 = self.pos - self.obj2.pos
                    vel2 += self.obj2.angvel % s2   
                    
                #one clever trick!
                vf = vel1-vel2
                vavg = (vi + vf)/2
                dK = impulse*vavg
                if dK > 0:
                    mu = 0
                    Jt = 0
                    Jn = dvn/Wtt
                    impulse = Jn*n + Jt*t
                # print(dK)
                
                if Jn > 0:
                    return impulse
                else:
                    #do nothing
                    1 + 1
                # return impulse # returned so that it can be visualized
        
class ContactGenerator:
    def __init__(self, objects, restitution=None, friction=None, static=None):#restitution=0, friction=0, static=None):
        self.objects = objects
        if restitution is None:
            restitution = 0
            print("Restitution not given.  Assuming zero restitution.")
        if friction is None:
            friction = 0
            print("Kinetic friction coefficient not given.  Assuming zero.")
        if static is None:
            static = friction
            print("Static friction coefficient not given.  Assuming the same as kinetic.")
        else:
            if static < friction:
                print("Warning, static friction coefficient must not be less than kinetic; setting it equal to kinetic.")
                static = friction
        self.data = {'restitution': restitution,
                     'friction': friction,
                     'static': static}
        for o in objects:
            o.contacts.append(self)
        self.contacts = []
    
    def remove(self, obj):
        self.objects.remove(obj)
        
    def contact_all(self):
        self.contacts.clear()
        # Loop through all pairs once
        for i in range(1, len(self.objects)):
            obj1 = self.objects[i]
            for j in range(i):
                obj2 = self.objects[j]
                if isinstance(obj1, Circle) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_circle(obj1, obj2, self.data))
                elif isinstance(obj1, Circle) and isinstance(obj2, Wall):
                    self.contacts.extend(contact_circle_wall(obj1, obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_wall(obj2, obj1, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Wall):
                    pass
                elif isinstance(obj1, Circle) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_circle_polygon(obj1, obj2, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Circle):
                    self.contacts.extend(contact_circle_polygon(obj2, obj1, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Wall):
                    self.contacts.extend(contact_polygon_wall(obj1, obj2, self.data))
                elif isinstance(obj1, Wall) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_polygon_wall(obj2, obj1, self.data))
                elif isinstance(obj1, Polygon) and isinstance(obj2, Polygon):
                    self.contacts.extend(contact_polygon_polygon(obj1, obj2, self.data))
                else:
                    print("Warning! ContactGenerator not implemented between ",
                          type(obj1)," and ", type(obj2),".")
        return self.contacts

def contact_circle_circle(obj1, obj2, restitution=0):
    r = obj1.pos - obj2.pos
    rmag2 = r.mag2()
    R = obj1.radius + obj2.radius
    if rmag2 < R*R:
        rmag = sqrt(rmag2)
        penetration = R - rmag
        normal = r/rmag
        return [(Contact(obj1, obj2, restitution, normal, penetration))]
    return []

def contact_circle_wall(obj1, obj2, restitution=0):
    R = obj2.pos * obj1.normal - obj1.pos * obj1.normal
    if R > -obj2.radius:
        #print("Eat Bug")
        penetration = obj2.radius - (obj1.pos - obj2.pos) * obj1.normal
        return[(Contact(obj1, obj2, restitution, obj1.normal, penetration))]
    return []
        
def contact_circle_polygon(obj1, obj2,restitution=0):
    # obj1 is polygon
    # obj2 is circle
    least_p = 1e99
    p = vector2(0,0)
    for i in range(len(obj1.points)):
        p = (obj1.points[i] - obj2.pos) * obj1.normals[i] + obj2.radius
        if p <= 0:
            # exit
            return []
        if p < least_p:
            least_p = p
            least_n = obj1.normals[i]
       
    # loop over all vertices; find nearest, rv
    least_d2 = 1e99
    
    for o in obj1.points:
        d2 = (o - obj2.pos).mag2()
        if d2 < least_d2:
            least_d2 = d2
            least_r = o
    n_hat = (least_r - obj2.pos).hat()
    wall_pos = obj2.pos + obj2.radius * n_hat
    max_p = -1e99
    for r in obj1.points:
        # one of these isn't a vector2!?
        # we know that n_hat is a vector2 so...?
        # print("p is a ", p.type)
        # should be p not r but for some reason p is not working
        p = (wall_pos - r)*n_hat
        if p > max_p:
            max_p = p
    
    #r = obj2.pos + obj2.radius*n_hat
    #p = (r - least_r) * n_hat
        
    if max_p < 0:
        return []
    if max_p < least_p:
        return[(Contact(obj1, obj2, restitution,  n_hat, max_p))]
    else: # circle side
        return[(Contact(obj2, obj1, restitution, least_n, least_p))]
        
def contact_polygon_polygon(obj1, obj2, data):
    # print("Contact between polygons!")
    # from notes
    # find deepest corner penetration from all walls on each polygon
    least_p = float('inf')
    for i in range (len(obj1.points)):
        # find max penetration and point that gives it, for each wall on other object
        max_p = -float('inf')
        for r in obj2.points:
            p = (obj1.points[i] - r)*obj1.normals[i]
            if p > max_p:
                max_p = p
                max_r = r
        if max_p < 0:
            # this means they ain't touchin, yo
            return []
        # find side with least penetration (among the maxes) and make contact from this
        if max_p < least_p:
            least_p = max_p
            contact = Contact(obj2, obj1, data, obj1.normals[i], least_p, max_r)
    # repeat for other shape
    for i in range (len(obj2.points)):
        # find max penetration and point that gives it, for each wall on other object
        max_p = -float('inf')
        for r in obj1.points:
            p = (obj2.points[i] - r)*obj2.normals[i]
            if p > max_p:
                max_p = p
                max_r = r
        if max_p < 0:
            # this means they ain't touchin, yo
            return []
        # find side with least penetration (among the maxes) and make contact from this
        if max_p < least_p:
            least_p = max_p
            contact = Contact(obj1, obj2, data, obj2.normals[i], least_p, max_r)        
    return [contact]
    
def contact_polygon_wall(obj1, obj2, data):
    #1 is polygon, 2 is wall
    # find max penetration and point that gives it, for each wall on other object
    max_p = -1e39
    for r in obj1.points:
        p = (obj2.pos - r)*obj2.normal
        if p > max_p:
            max_p = p
            max_r = r
    if max_p <= 1e-39:
        # this means they ain't touchin, yo
        return []   
    return [Contact(obj1, obj2, data, obj2.normal, max_p, max_r, obj2.pos)]
  
