# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 14:49:29 2019

@author: Student

Let's make chomp!
"""

#get the initial number from the user

topnum = int(input("Welcome to chomp! Player 1, pick initial number: "))

print("The number is: " , topnum)

# fill a list with all divisors of that number

numlist = []
removelist = []

for n in range(1, topnum + 1):
    if topnum % n == 0:
        numlist.append(n)
print("Here is the list!", end="")
print(numlist)

#take turns choosing a number to remove
#remove that number and each multiple of it in the list
#loser is the person left with one
#note, picking a prime right away is a load of bungus

#step one, player loop

player = 2

gameover = False
while gameover == False:
    #player one's turn
    if player == 1:
        remove = int(input("Player one, pick a number: "))
        if remove == 1:
            break
        if numlist.__contains__(remove):
            for n in numlist:
                if n % remove == 0:
                    #print(n, "Added to removal list")
                    removelist.append(n)
            for r in removelist:
                #print(r, "Removed")
                numlist.remove(r)
            removelist.clear()
        print("Here is the list!", end="")
        print(numlist)    
        player = 2
    #player two's turn
    elif player == 2:
        remove = int(input("Player two, pick a number: "))
        if remove == 1:
            break
        if numlist.__contains__(remove):
            for n in numlist:
                if n % remove == 0:
                    #print(n, "Added to removal list")
                    removelist.append(n)
            for r in removelist:
                #print(r, "Removed")
                numlist.remove(r)
            removelist.clear()
        print("Here is the list!", end="")
        print(numlist) 
        player = 1
    # check to see if one is the only number left
    if numlist.__len__() <= 1:
        gameover = True
#that's all, folks!
print("Game is over! Player ", player, " Loses!")