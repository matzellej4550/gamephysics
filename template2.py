# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 14:28:21 2019

@author: sinkovitsd
"""

# import statements
import pygame
from random import randint, uniform, shuffle
from math import pi, sin, cos, acos, asin, atan2
from circle import Circle
from vector2 import vector2
import force
import contact
from wall import Wall
from polygon import Polygon

# colors
WHITE = (255,255,255)
BLACK = (0,0,0)
GREEN = (0, 255, 0)

# entire game
def main():
    # initiate the game
    pygame.init()
    # establish screen
    screen = pygame.display.set_mode([800,600])
    
    # list of objects in world
    objects = []
    # list of forces active
    forces = []
    # click forces to only be called once
    clickForces = []
    # list of contact generators
    contact_generators = []
    # list of contacts to be resolved
    contacts = []
    
    # this is the golf ball!
    objects.append(Circle(1, vector2(50, 50), vector2(0,0), 20, WHITE))
    
    # bottom wall
    objects.append(Wall(vector2(400,600), vector2(0,1), (127,127,127), 400))
    # left wall
    objects.append(Wall(vector2(0,300), vector2(-1,0), (127,127,127), 300))
    # right wall
    objects.append(Wall(vector2(800,300), vector2(1,0), (127,127,127), 300))
    # top wall
    objects.append(Wall(vector2(400,0), vector2(0,-1), (127,127,127), 400))
    
    # Wall by Hole polygon - its a square!
    objects.append(Polygon(vector2(500, 400),[vector2(500, 400), vector2(600, 400), vector2(600, 600), vector2(500,600)], (0,0,0)))
    
    # first wall
    objects.append(Polygon(vector2(200, 0),[vector2(200, 0), vector2(300, 0), vector2(300, 400), vector2(200,400)], (0,0,0)))
    
    # Circle object to collide with
    objects.append(Circle(0, vector2(500,300), vector2(0,0), 50, (0,50,255)))
     
    # drag force on ball so it eventually stops
    dragc = 0.6
    forces.append(force.SingleForce(objects, lambda o: force.constant_drag(o, dragc)))
    
    # create contact generator object
    contact_generators.append(contact.ContactGenerator(objects, 0.5))    

    # build a wall?
    
    # Main Loop
    done = False
    frame_rate = 60
    dt = 1.0/frame_rate

    # hill height - strong
    hillStrength = 5
    
    # hill angle - straight up
    hillAngle = 3*pi/2
    
    # hill pos
    hillx1 = 600
    hillx2 = 800
    hilly1 = 400
    hilly2 = 500
    
    #hole pos
    holex = 750
    holey = 550
    
    #hole buffer
    holebuffer = 30
    
    # turf pos
    turfx1 = 300
    turfx2 = 800
    turfy1 = 0
    turfy2 = 200

    #win boolean
    win = False

    clock = pygame.time.Clock()
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if (event.type == pygame.QUIT # If user clicked close
                or (event.type == pygame.KEYDOWN 
                    and event.key == pygame.K_ESCAPE)): # or pressed ESC
                done = True # Flag that we are done so we exit this loop
            # left mouse click
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and objects[0].vel.x == 0 and objects[0].vel.y == 0:
                # add a force to objects[0] (the ball)
                mouseVector = vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1])
                # get the vector between the mouse and the ball
                u = (mouseVector-objects[0].pos).hat()
                # get angle between mouse and ball
                angle = atan2(u.y, u.x)
                # apply a force onto the ball given the stength and angle
                clickForces.append(force.SingleForce(objects, lambda o: force.golf_force(o, ((pygame.mouse.get_pos()[0] - objects[0].pos.x)**2 + (pygame.mouse.get_pos()[1] - objects[0].pos.y)**2)**(1/2), angle)))
    
        # Add forces
        for f in forces:
            f.force_all()
            
        # click forces should only happen once
        for c in clickForces:
            c.force_all()
            clickForces.pop()
        
        # Move objects
        for o in objects:
            o.integrate(dt)
        
        # Get contacts
        niterations = 0
        max_iterations = 10
        while niterations < max_iterations:
            niterations += 1
            contacts = []
            for g in contact_generators:
                contacts.extend(g.contact_all())
              
            if len(contacts)==0:
                break
                
            # Resolve contacts
            contacts.sort(key=lambda x: x.penetration)
            for c in contacts:
                c.resolve()

        # Draw objects to screen
        #600, 500, 100, 50
        screen.fill(GREEN) # clears the screen
        
        #hole
        pygame.draw.circle(screen, (100,100,100), vector2(holex,holey), 25)
        
        # draw hill
        pygame.draw.polygon(screen, (150,210,150), (vector2(hillx1, hilly1),vector2(hillx2,hilly1),vector2(hillx2,hilly2),vector2(hillx1,hilly2)))
        
        # draw turf
        pygame.draw.polygon(screen, (170,250,150), (vector2(turfx1, turfy1),vector2(turfx2,turfy1),vector2(turfx2,turfy2),vector2(turfx1,turfy2)))
        
        # draw arrow that points down hill
        midy = (hilly2 - hilly1) / 2
        midx = (hillx2 - hillx1) / 2
        '''
             5
        
        6 7    3  4
        
          1    2
        '''
        pygame.draw.polygon(screen, (250,50,0), (vector2(hillx1 + 50, hilly2), vector2(hillx2 - 50, hilly2), vector2(hillx2 - 50, hilly1 + midy), vector2(hillx2, hilly1 + midy),
                                     vector2(hillx1 + midx, hilly1), vector2(hillx1, hilly1 + midy), vector2(hillx1 + 50, hilly1 + midy)))
        
        for o in objects:
            o.draw(screen)
            
        # Draw aiming line
        pygame.draw.line(screen, (0,0,0), vector2(objects[0].pos.x, objects[0].pos.y), vector2(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]), 3)
            
        # Hole   
        if (objects[0].pos.x > holex-holebuffer + objects[0].radius and objects[0].pos.y > holey-holebuffer + objects[0].radius and objects[0].pos.x < holex+holebuffer - objects[0].radius and objects[0].pos.y < holey + holebuffer - objects[0].radius):
            #get angle between ball and hole
            holevector = (objects[0].pos - vector2(holex, holey)).hat()
            dangle = atan2(holevector.y, holevector.x)
            
            # force into the hole
            if win == False:    
                clickForces.append(force.SingleForce(objects, lambda o: force.golf_force(o, -10, dangle)))
            
            # Check for win
            if objects[0].vel.mag() == 0 and win == False:
                print("WINNER ")
                win = True
            
            # Stops the ball from moving once it is in the hole
            if win == True:
                objects[0].vel = vector2(0,0)
                objects[0].pos = vector2(holex, holey)
        # Hill
        if (objects[0].pos.x > hillx1 and objects[0].pos.y > hilly1 and objects[0].pos.x < hillx2 and objects[0].pos.y < hilly2):
            # Apply a force
            clickForces.append(force.SingleForce(objects, lambda o: force.golf_force(o, hillStrength, hillAngle)))
        
        # turf
        if (objects[0].pos.x > turfx1 and objects[0].pos.y > turfy1 and objects[0].pos.x < turfx2 and objects[0].pos.y < turfy2 and objects[0].vel.mag() > 2):
            # Apply a force
            clickForces.append(force.SingleForce(objects, lambda o: force.golf_force(o, -0.01 * objects[0].vel.mag(), atan2(objects[0].vel.hat().y, objects[0].vel.hat().x))))
        
        # Update the screen
        pygame.display.update()

        # --- Limit to 60 frames per second
        clock.tick(60)
        
    pygame.quit()
 
""" Safe start """
if __name__ == "__main__":
    try:
        main()
    except:
        pygame.quit()
        raise